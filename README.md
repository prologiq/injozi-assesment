# InJozi Assesment
### Steps to run the application successfully(open 2 termina windows)
1. Run Node via npm
```bash
npm run start-watch
```

2. Navigate to localhost:3000 to check if the API is running

3. Start up Angular Project, navigate to the director 'angular-client-app'
```bash
npm run start-watch
```

## Instructions on running the Web Application
1. Navigate to the root directory through your terminal window and type `npm start` or to start the server. The server should output on the window that it is running from port 3000. You can navigate to a browser window and type `localhost:3000` to see the results.

We also have `nodemon` configured to monitor changes to certain file/folders. In this instance, we have it monitoring the `index.js` file for changes. To execute this option instead of the `npm start` you need to execute the following command instead.
```bash
npm run start-watch
```

2. navigate to the 'angular-client-app' directory to execute the front-end of the application(this can be done through another terminal window)
- 


## Middleware that has been setup
1. log


## Access Keys
### MongoDB Details
```
server = 'node-mongo-rest-yzcl9.mongodb.net'
database = 'node-mongo-rest'
username = 'sammydev'
password = 'W3Ar3Gr00t'
```


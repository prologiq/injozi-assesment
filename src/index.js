let express = require('express')
let app = express()
let personRoute = require('./routes/person')
let customerRoute = require('./routes/customer')
let path = require('path')
let bodyParser = require('body-parser')


app.use(bodyParser.json())

// logging through the console
app.use((req, res, next) =>{
    console.log(`${new Date().toString()} => ${req.originalUrl}`, req.body)
    next()
})
app.use(personRoute)
app.use(customerRoute)
app.use(express.static('public'))

// error handler - 404
app.use((req, res, next) => {
    res.status(404).send('404 Error - You seem to be lost')
})

// error handler - 500
app.use((err, req, res, next) => {
    console.error(err.stack) // prints out the error stack to the console
    res.sendFile(path.join(__dirname, '../public/500.html'))
})

// Connect to MongoDB Database
// Connect to DB
// mongoose.connect(‘mongodb://localhost/mycargarage’)
//  .then(() => console.log(‘MongoDB connected…’))
//  .catch(err => console.log(err))

const PORT = process.env.PORT || 3000
app.listen(PORT, () => console.info(`Server has started on ${PORT}`))
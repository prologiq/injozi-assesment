let mongoose = require('mongoose')

const server = 'node-mongo-rest-yzcl9.mongodb.net'
const database = 'node-mongo-rest'
const username = 'sammydev'
const password = 'W3Ar3Gr00t'

// mongodb+srv://sammydev:<password>@node-mongo-rest-yzcl9.mongodb.net/test?retryWrites=true
// mongodb+srv://sammydev:<password>@node-mongo-rest-yzcl9.mongodb.net/test?retryWrites=true
mongoose.connect(`mongodb+srv://${username}:${password}@${server}/${database}`)

let CustomerSchema = new mongoose.Schema({
    name: String,
    lastname: String,
    email: {
        type: String,
        required: true,
        unique: true
    },
    contactNum: String
})

module.exports = mongoose.model('Customer', CustomerSchema)
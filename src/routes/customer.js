let CustomerModel = require('../models/customer.model')
let express = require('express')
let router = express.Router()

//Create a new customer
// POST request via localhost:3000/customer
router.post('/customer', (req, res)=>{
    if(!req.body){
        return res.status(400).send('Request body is missing')
    }

    // let user = {
    //     name: 'firstname',
    //     lastname: 'lastname',
    //     email: 'test@user.com',
    //     contactNum: '012 345 6789'
    // }

    let model = new CustomerModel(req.body)
    model.save()
        .then(doc => {
            if(!doc || doc.length === 0)
            {
                return res.status(500).send(doc)
            }
            res.status(201).send(doc) // Meaning that the document has been created successfully
        })
        .catch(err => {
            res.status(500).json(err)
        })

})

// Attempting to get all records without any provided argument
// router.get('/customer/all', (req, res) =>{
//     return releaseEvents.status(400).send('Lets see if we can return all the records')
// })

// Ability to GET the data
router.get('/customer', (req, res) => {
    // Check if email exists
    if(!req.query.email)
    {
        return res.status(400).send('Missing url parameter: email')    
    }
    
    CustomerModel.findOne({
        email: req.query.email
    })
        .then(doc => {
            res.json(doc)
        })
        .catch(err =>
            {
                res.status(500).json(err)
            })
})

// Ability to UPDATE existing customer
router.put('/customer', (req, res) => {
    // Check if email exists
    if(!req.query.email)
    {
        return res.status(400).send('Missing url parameter: email')    
    }

    CustomerModel.findOneAndUpdate({
        email: req.query.email
    }, req.body, {
        new: true
    })
        .then(doc => {
            res.json(doc)
        })
        .catch(err =>
            {
                res.status(500).json(err)
            })
})

// Ability to Delete a customer
router.delete('/customer', (req, res) => {
    // Check if email exists
    if(!req.query.email)
    {
        return res.status(400).send('Missing url parameter: email')    
    }

    CustomerModel.findOneAndRemove({
        email: req.query.email
    })
        .then(doc => {
            res.json(doc)
        })
        .catch(err =>
            {
                res.status(500).json(err)
            })
})

module.exports = router
let express = require('express')
let router = express.Router()

// Query Property on the request object(QueryString)
// e.g replace localhost:3000/person/?name=sammy&age=30
router.get('/person', (req, res) =>{
    if(req.query.name)
    {
        res.send(`You have requested a person: ${req.query.name}`)
    }
    else{
        res.send('You have requested a person')
    }
})

// Params property on the request object 
// e.g replace localhost:3000/person/sammy
router.get('/person/:name', (req, res) =>{
    res.send(`You have requested a person: ${req.params.name}`)
})

// Handler router for the error 500
router.get('/error', (req, res) =>{
    throw new Error('this is an intentional error')
})

module.exports = router